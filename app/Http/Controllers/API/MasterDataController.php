<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\MasterData;
use Illuminate\Http\Request;

class MasterDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->input('title')){
            return response()->json('Please add masterdata title', 400);
        }

        if(!$request->input('category_id')){
            return response()->json('Please add masterdata category', 400);
        }

        MasterData::create([
            'title' => $request->input('title'),
            'category_id' => $request->input('category_id'),
            'parent_category_id' => $request->input('parent_category_id'),
            'is_parent' => $request->input('is_parent'),
            'is_active' => $request->input('is_active'),
        ]);

        return response()->json('Masterdata created successfully', 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $body = MasterData::where('is_active', true)->where('category_id', $id)->get();
        
        if($body) {
            return response()->json($body, 200);
        }
        else {
            return response()->json('no_items_found', 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = MasterData::where('id', $id)->update([
            'title' => $request->input('title')
        ]);

        if($category){
            return response()->json('Masterdata updated successfully', 200);
        }
        else {
            return response()->json('Masterdata not found', 404);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createSub(Request $request)
    {
        dd($request->all());
    }
}
