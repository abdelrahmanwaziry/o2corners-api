<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User as ModelsUser;
use App\Models\VerificationCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use Twilio\Rest\Client as TwilioClient;
use GuzzleHttp\Exception\RequestException;

class UserController extends Controller
{

    public function authenticate(Request $request){
        $rules = [
            'mobile_number' => 'required',
            'password' => 'required', 
        ];
        
        $user = ModelsUser::where('mobile_number', $request->input('mobile_number'))->first();
        
        $is_verified = $user->is_verified;
        
        if(!$is_verified){
            return response()->json('User is not verified', 400);
        }

        $valid = $this->validate($request, $rules);
        
        
        if(!$valid){
            return response()->json($this->errors, 400);
        }

        $request->merge(['username' => $request->input('mobile_number')]);


        $body = $this->oauth($request->all());

        
        return response()->json($body, 200);
        
    }

    public function profile(Request $request){
        $body = ModelsUser::findOrfail($request->user()->id);
        return response()->json($body, 200);
    }

    private function oauth($parameters, $method='POST', $route='oauth/token', $headers=[]){
        $http = new Client(['protocols' => ['http', 'https']]);
        try {
            $response = $http->request($method, url($route), [
                'form_params' => $parameters,
                'headers'   => $headers
            ]);

            $body = json_decode($response->getBody(), true);
            
            return $body;
        }
        catch(RequestException $e) {
            return json_decode($e->getResponse()->getBody(), true);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        
        $rules = [
            'name' => 'required|max:250|min:5',
            'email' => 'required|email|unique:users,email|max:255',
            'password' => 'required|max:250|min:6',
        ];

        $otpString = '0123456789';
        
        $randomString =  substr(str_shuffle($otpString), 1, 6);

        $isValid = $this->validate($request, $rules);
        
        if(!$isValid){
            return response()->json($this->errors, 400);
        }
        else {
            $user = ModelsUser::create([
                'name' =>  $request->input('name'),
                'password' => Hash::make($request->input('password')),
                'email' => $request->input('email'),
                'phone_number' => $request->input('phone_number'),
                'mobile_number' => $request->input('mobile_number'),
                'company_name' => $request->input('company_name'),
                'address' => $request->input('address'),
                'zip_code' => $request->input('zip_code'),
                'country' => $request->input('country'),
                'city' => $request->input('city'),
                'gender' => $request->input('gender'),
                'is_dealer' => $request->input('is_dealer'),
                'is_active' => $request->input('is_active'),
                'is_verified' => false,
            ]);

            $vcode = VerificationCode::create([
                'mobile_number' => $request->input('mobile_number'),
                'code' => $randomString
            ]);
            
            $vcode->save();
            
            $this->sendSMS('A new verification code has been sent to your account ' . $randomString, $request->input('mobile_number'));
            $body = $user->only('id', 'name', 'email', 'mobile_number');
            return response()->json($body, 201);
            }
        }
        
    public function verify(Request $request)
    {
        $storedCode = VerificationCode::where('mobile_number', $request->input('mobile_number'))->orderBy('id', 'desc')->first();
        $sentCode = $request->input('code');

        if($storedCode->code === $sentCode){
            ModelsUser::where('mobile_number', $request->input('mobile_number'))->update([
                'is_verified' => true
            ]);
            return response()->json('Your account has been successfully verified', 200);
        }
        else {
            return response()->json('Verification code is invalid', 400);
        }
    }
    
    public function resendCode(Request $request)
    {
        $otpString = '0123456789';
        $randomString =  substr(str_shuffle($otpString), 1, 6);
        $vcode = VerificationCode::create([
            'mobile_number' => $request->input('mobile_number'),
            'code' => $randomString
        ]);
            
        $vcode->save();
        $this->sendSMS('A new verification code has been sent to your account ' . $randomString, $request->input('mobile_number'));
        
        return response()->json('A new verification code has been sent to your account', 200);
        
    }

    private function sendSMS($message, $recipients)
    {
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_TOKEN");
        $twilio_number = getenv("TWILIO_FROM");
        $client = new TwilioClient($account_sid, $auth_token);
        $client->messages->create($recipients, 
            ['from' => $twilio_number, 'body' => $message] );
    }
}
