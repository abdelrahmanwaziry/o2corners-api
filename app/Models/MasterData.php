<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterData extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'category_id',
        'parent_category_id',
        'is_parent',
        'is_active'
    ];
}
