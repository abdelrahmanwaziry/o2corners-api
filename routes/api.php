<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function(){
    Route::post('/register', 'App\Http\Controllers\API\UserController@store');
    Route::post('/login', 'App\Http\Controllers\API\UserController@authenticate');
    Route::post('/verify', 'App\Http\Controllers\API\UserController@verify');
    Route::post('/resendCode', 'App\Http\Controllers\API\UserController@resendCode');
    Route::middleware('auth:api')->group(function(){
        Route::get('/profile', 'App\Http\Controllers\API\UserController@profile');
        Route::post('/update', 'App\Http\Controllers\API\UserController@update');
        Route::prefix('category')->group(function(){
            Route::get('/all', 'App\Http\Controllers\API\CategoriesController@index');
            Route::post('/create', 'App\Http\Controllers\API\CategoriesController@store');
        });
        Route::prefix('masterData')->group(function(){
            Route::get('/all', 'App\Http\Controllers\API\MasterDataController@index');
            Route::post('/create', 'App\Http\Controllers\API\MasterDataController@store');
            Route::get('/{id}', 'App\Http\Controllers\API\MasterDataController@show');
            Route::post('/update/{id}', 'App\Http\Controllers\API\MasterDataController@update');
        });
    });
});
